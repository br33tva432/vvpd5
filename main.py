import numpy as np


def graph_vvod(graph, numb):
    """
    Ввод значений в матрицу

    Функция делает заполнение взвешенного графа более удобным,
    так как вводить надо не все значения, а лишь верхний треугольник.
    Принимает на вход пустой граф и количество вершин.
    Возвращает заполненной граф.

    Args:
        graph: матрица с вершинами, но без значений
        numb: число вершин

    Returns:
        Заполненный граф

    Raises:
        ValueError
    """
    j = 1
    print('Введите значения из верхней треугольной матрицы без главной диагонали'
          '\nБесконечность взять за нуль')
    while 1 <= j < numb - 1:
        try:
            values = list(map(int, input("Введите значения: ").split()))
            if len(values) != len(graph[j][j + 1:numb]):
                print('Введено неверное количество значений в строке')
                continue
            if bool(list(filter(lambda x: x < 0, values))):
                print('Введено отрицательное значение')
                continue
            graph[j][j + 1:numb] = values
        except ValueError:
            print('Неверный тип данных, введите целые числа')
            continue
        for k in range(j, numb):
            if graph[j][k] == 0:
                graph[j][k] = float('+inf')
            graph[k][j] = graph[j][k]
        j += 1
    print(*graph, sep='\n')
    return graph


def task(graph):
    """
    Алгоритм Дейкстры

    Функция реализует алгоритм Дейкстры.
    Принимает граф в виде numpy-матрицы,
    возвращает кратчайшую длину пути и номера вершин на этом пути

    Args:
        graph: Numpy-матрица, граф

    Returns:
        Кортеж с кратчайшей длиной пути и номерами вершин этого пути
    Raises:
        ValueError

    Examples:
        >>> a = float('+inf')
        ... task(np.array([[0, 1, 2, 3], [1, a, 2, a], [2, 2, a, 1], [3, a, 1, a]]))
        1 3
        3, (1, 2, 3)
    """
    idx = list()
    while True:
        try:
            print('Введите через пробел начальный и конечный узел: ')
            node1, node2 = map(int, input().split())
        except ValueError:
            print("Нужно ввести два целых числа")
            continue
        if node2 > len(graph) or node1 <= 0 or node1 > node2:
            print("Номера узлов выходят за границы")
            continue
        ind_path = node1,
        path = graph[node1]
        for k in range(node1 + 1, node2 + 1):
            for j in range(node1 + 1, node2 + 1):
                prev_path = path[j]
                path[j] = min(path[j], path[k] + graph[k][j])
                if path[j] != prev_path:
                    idx.append((k, j))
        prev_ind = node2
        for q in idx[::-1]:
            if q[1] == prev_ind:
                ind_path += q[0],
                prev_ind = q[0]
        ind_path += node2,
        break
    return path[-1], ind_path


def main():
    """
    Тело программы

    Функция, обеспечивающая взаимодействие остальных функций.

    Raises:
        ValueError
    """
    a = 1
    while a != 0:
        try:
            numb = int(input("Введите количество вершин в графе: "))
        except ValueError:
            print("Неверный формат введенных данных")
            numb = 0
        if numb == 0:
            continue
        else:
            numb += 1
            a = 0
    graph = np.zeros((numb, numb))
    graph[0] = [q for q in range(numb)]
    for i in range(1, numb):
        graph[i] = [i] + list(map(int, '0' * (numb - 1)))
    print(*graph, sep='\n')
    graph = graph_vvod(graph, numb)
    print(task(graph))


if __name__ == "__main__":
    main()
